
const Video = require('./models').Video;

module.exports = router => {
    router.get('/count', (req, res) => {
        console.log('hello count')
        const query = {};
        Object.keys(req.query).forEach(key => {
            if (req.query[key]) {
                query[key] = req.query[key];
                if (key === 'title') {
                    query[key] = new RegExp(query[key], 'ig');
                }
            }
        })

        Video.count(query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })

    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
            // Regex search title
            if (key === 'title') {
                rest[key] = new RegExp(rest[key], 'ig');
            }
        })
        if (one) {
            q = Video.findOne(rest);
        } else {
            q = Video.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((videos) => {
            return res.json(videos);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        Video.create(data)
            .then((video) => {
                return res.json(video);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        Video.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => Video.find(conditions))
            .then(videos => {
                return res.json(videos);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let videos;
        Video.find(conditions)
            .then((a) => {
                videos = a;
                return Video.remove(conditions)
            })
            .then(() => {
                return res.json(videos);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        Video.findById(req.params.id)
            .then((video) => {
                return res.json(video);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Video.findByIdAndUpdate(id, { $set: changes })
            .then(() => Video.findById(id))
            .then(video => {
                return res.json(video);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedVideo;
        Video.findById(id)
            .then(video => {
                deletedVideo = video;
                return Video.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedVideo);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })


    return router;
}