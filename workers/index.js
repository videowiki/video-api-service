const AUDIO_PROCESSOR_API_ROOT = process.env.AUDIO_PROCESSOR_API_ROOT;

module.exports = ({ rabbitmqChannel }) => {
  const exporterWorker = require("@videowiki/workers/exporter")({
    rabbitmqChannel,
  });
  const transcriberWorker = require("@videowiki/workers/transcriber")({
    rabbitmqChannel,
  });
  const spleeterWorker = require("@videowiki/workers/spleeter")({
    rabbitmqChannel,
  });
  const audioProcessorWorker = require("@videowiki/workers/audio_processor")({
    rabbitmqChannel,
    AUDIO_PROCESSOR_API_ROOT,
  });
  const whatsappBotWorker = require("@videowiki/workers/whatsapp_bot")({
    rabbitmqChannel,
  });
  const automaticVideoBreakWorker = require('@videowiki/workers/automatic_video_break')({ 
    rabbitmqChannel
  })

  return {
    exporterWorker,
    transcriberWorker,
    spleeterWorker,
    whatsappBotWorker,
    audioProcessorWorker,
    automaticVideoBreakWorker,
  };
};
