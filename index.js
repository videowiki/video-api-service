const mongoose = require('mongoose');
const fs = require('fs');
const videowikiGenerators = require('@videowiki/generators');
const requiredDirs = ['tmp'];
const { server, app, createRouter } = require('./generateServer')();
const dbRoutes = require('./dbRoutes');
const middlewares = require('./middlewares');
const validation = require('./validate_request');

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
var upload = multer({ storage: storage })

requiredDirs.forEach(d => {
    if (!fs.existsSync(d)) {
        fs.mkdirSync(d);
    }
})

// initiate database connection
const DB_CONNECTION = process.env.VIDEO_SERVICE_DATABASE_URL;
let mongoConnection;
const rabbitmqService = require('@videowiki/workers/vendors/rabbitmq');
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
let rabbitmqChannel;

mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    // initiate rabbitmq connection
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })
        // healthcheck route
        videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection, rabbitmqConnection: rabbitmqChannel.connection })
        const workers = require('./workers')({ rabbitmqChannel: channel });
        
        const controller = require('./controller')({ workers });
        require('./rabbitmqHandlers').init({ channel, workers })
        // internal database routes 
        app.use('/db', dbRoutes(createRouter()))

        app.all('*', (req, res, next) => {
            if (req.headers['vw-user-data']) {
                try {
                    const user = JSON.parse(req.headers['vw-user-data']);
                    req.user = user;
                } catch (e) {
                    console.log(e);
                }
            }
            next();
        })

        app.get('/whatsapp-bot', controller.getVideoForWhatsApp);

        app.get('/', controller.getVideos)
        app.get('/count', controller.getVideosCount)

        app.post('/upload', upload.any(), middlewares.authorizeUploadVideo, validation.create_video, controller.uploadVideo)

        app.patch('/:id/backgroundMusic', upload.any(), middlewares.authorizeUploadVideo, controller.uploadBackgroundMusic)
        app.post('/:id/backgroundMusic/extract', middlewares.authorizeVideoAdmin, controller.extractVideoBackgroundMusic)
        app.delete('/:id/backgroundMusic', middlewares.authorizeVideoAdmin, controller.deleteBackgroundMusic)


        app.post('/:id/convert', middlewares.authorizeAdminAndReviewer, controller.convertVideo);
        app.post('/:id/refreshMedia', middlewares.authorizeAdminAndReviewer, controller.refreshMedia);

        app.post('/:id/automaticBreak', controller.automaticCutVideo)
        app.post('/all/transcribe', controller.transcribeAllVideos);
        app.post('/:id/transcribe', middlewares.authorizeAdminAndReviewer, controller.transcribeVideo)
        app.post('/:id/transcribe/skip', middlewares.authorizeAdminAndReviewer, controller.skipTranscribe)

        app.put('/:id/reviewers', middlewares.authorizeVideoAdmin, controller.updateReviewers)
        app.post('/:id/reviewers/resendEmail', middlewares.authorizeVideoAdmin, controller.resendEmailToReviewer)

        app.put('/:id/verifiers', middlewares.authorizeVideoAdmin, controller.updateVerifiers)
        app.post('/:id/verifiers/resendEmail', middlewares.authorizeVideoAdmin, controller.resendEmailToVerifier)

        app.put('/:id/projectLeaders', middlewares.authorizeVideoAdmin, controller.updateProjectLeaders)

        app.put('/:id/folder', middlewares.authorizeVideoAdmin, controller.updateFolder);

        app.get('/:id', controller.getVideoById);
        app.patch('/:id', upload.any(), middlewares.authorizeVideoAdmin, controller.updateVideo);
        app.delete('/:id', middlewares.authorizeOwnerAndAdmin, controller.deleteVideo)

    });
})
.catch(err => {
    console.log('error mongo connection', err);
    process.exit(1);
})



const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
